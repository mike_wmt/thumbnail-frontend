import './index.scss';
import { Row, Col, Form, Button, Upload, message, Select, Typography } from 'antd';
import { useState } from 'react';
import axios from 'axios';
import GeneratedThumbnail from '../components/generatedThumbnail/GeneratedThumbnail';
const { Option } = Select;
const { Title } = Typography;

const App = props => {
  const [fileList, setFileList] = useState([]);
  const [thumbnailURL, setThumbnailURL] = useState({ thumbnailLink: null });
  const [form] = Form.useForm();
  const resolutionOptions = [10, 20, 50];

  const dummyRequest = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess('ok');
    }, 0);
  };

  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  const onFinish = async values => {
    values.image = values.image?.fileList[0]?.originFileObj || null;
    const formData = new FormData();
    for (const property in values) {
      formData.append(`${property}`, values[property]);
    }
    const { data } = await axios({
      baseURL: 'https://thumbnail-backend.herokuapp.com/upload',
      method: 'POST',
      data: formData,
    });
    if (data.message === 'Thumbnail generated successfully') {
      form.resetFields();
      setFileList([]);
      setThumbnailURL({ thumbnailLink: data.thumbnailLink });
      return message.success(`${data.message}`);
    }
    message.error(`${data.message}`);
  };

  const layout = {
    labelCol: { xs: { span: 24 }, sm: { span: 12, offset: 9 } },
    wrapperCol: { xs: { span: 24 }, sm: { span: 24 } },
  };

  const tailLayout = {
    wrapperCol: { span: 24 },
  };

  return (
    <div className="uploadForm">
      <Row justify="center" align="middle">
        <Col xs={{ span: 22 }} md={{ span: 16 }} lg={{ span: 12 }}>
          <Title level={2}>Thumbnail Generater</Title>
          <Form
            form={form}
            {...layout}
            className="thumbnailGenerateForm"
            name="nest-messages"
            layout="vertical"
            initialValues={{ image: null }}
            onFinish={onFinish}
          >
            <Form.Item
              tooltip="supported formats are jpg, jpeg, png"
              label="Upload Image"
              name="image"
              rules={[{ required: true, message: 'Please upload an image' }]}
            >
              <Upload
                listType="picture-card"
                fileList={fileList}
                onChange={onChange}
                customRequest={dummyRequest}
                onPreview={onPreview}
              >
                {fileList.length < 1 && '+ Upload Image'}
              </Upload>
            </Form.Item>
            {/* <br /> */}
            <Form.Item
              tooltip="resolution is in pixel"
              label="Select Resolution"
              name="size"
              rules={[{ required: true, message: 'Please select a resolution' }]}
            >
              <Select
                showSearch
                allowClear
                style={{ width: 200 }}
                placeholder="Select Resolution"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {resolutionOptions.map(res => (
                  <Option value={res}>
                    {res} X {res}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <br />
            <Form.Item {...tailLayout}>
              <Button className="generateButton" type="primary" htmlType="submit">
                Generate Thumbnail
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
      {thumbnailURL.thumbnailLink ? (
        <GeneratedThumbnail thumbnail={thumbnailURL.thumbnailLink} />
      ) : null}
    </div>
  );
};
export default App;
