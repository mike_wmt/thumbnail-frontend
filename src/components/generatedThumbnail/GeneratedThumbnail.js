import './GeneratedThumbnail.scss';
import { Row, Col, Divider, Space, Typography } from 'antd';
const { Paragraph } = Typography;

const GeneratedThumbnail = ({ thumbnail }) => {
  return (
    <Row justify="center">
      <Col xs={{ span: 20 }} md={{ span: 12 }}>
        {thumbnail ? (
          <div className="generatedThumbnail">
            <Divider>
              <h3>Generated Thumbnail</h3>
            </Divider>
            <Space size="large" direction="vertical" align="start">
              <img src={thumbnail} alt="thumbnail" />
              <Paragraph copyable>{thumbnail}</Paragraph>
            </Space>
          </div>
        ) : null}
      </Col>
    </Row>
  );
};

export default GeneratedThumbnail;
